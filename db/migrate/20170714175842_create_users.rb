class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :platform, null: false
      t.string :team, null: false # will be same as platform except for Slack
      t.string :member # can be null if one user's account is created by another who only knows nickname, not userid
      t.string :nickname, null: false
      t.string :image_url
      t.json :omniauth
      t.timestamps
    end

    add_index :users, [:platform, :nickname], :unique => true

    create_table :keypairs do |t|
      t.references :user
      t.string :pubkey, null: false, unique: true
      t.string :encrypted_privkey, null: false
      t.string :encrypted_privkey_iv, null: false
      t.decimal :balance, null: false, default: 0, precision: 30, scale: 0
      t.timestamp :balance_at
      t.timestamps
    end
  end
end
