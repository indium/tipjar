class CreateOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :offers do |t|
      t.references :user, null: false
      t.string :title, null: false
      t.text :description
      t.string :category, null: false
      t.decimal :price, null: false, precision: 30, scale: 0
      t.datetime :discarded_at
      t.timestamps
    end

    add_index :offers, :discarded_at
  end
end
