# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2017_11_25_080754) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "keypairs", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "pubkey", null: false
    t.string "encrypted_privkey", null: false
    t.string "encrypted_privkey_iv", null: false
    t.decimal "balance", precision: 30, default: "0", null: false
    t.datetime "balance_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_keypairs_on_user_id"
  end

  create_table "offers", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "title", null: false
    t.text "description"
    t.string "category", null: false
    t.decimal "price", precision: 30, null: false
    t.datetime "discarded_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discarded_at"], name: "index_offers_on_discarded_at"
    t.index ["user_id"], name: "index_offers_on_user_id"
  end

  create_table "teams", id: :serial, force: :cascade do |t|
    t.string "team_id", null: false
    t.string "name", null: false
    t.boolean "active", default: true, null: false
    t.string "domain"
    t.string "token"
    t.boolean "gif_enabled", default: false, null: false
    t.integer "join_bonus", default: 0, null: false
    t.integer "max_user_join_bonus_count", default: 0, null: false
    t.integer "actual_user_join_bonus_count", default: 0, null: false
    t.integer "max_user_join_bonus_amount", default: 0, null: false
    t.integer "actual_user_join_bonus_amount", default: 0, null: false
    t.integer "max_admin_reward", default: 0, null: false
    t.integer "actual_admin_reward", default: 0, null: false
    t.integer "max_introducer_reward", default: 0, null: false
    t.integer "actual_introducer_reward", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "platform", null: false
    t.string "team", null: false
    t.string "member"
    t.string "nickname", null: false
    t.string "image_url"
    t.json "omniauth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["platform", "nickname"], name: "index_users_on_platform_and_nickname", unique: true
  end

end
