## Set up. Modify DATABASE_URL in commands below as per your set up:

You must have `ruby` installed already. Run `bundle` to install dependencies.

To drop existing database: `DATABASE_URL=postgresql://root:asd@localhost:5432/wallet rake db:drop`

To create the database: `DATABASE_URL=postgresql://root:asd@localhost:5432/wallet rake db:create`

To run database migrations (create tables etc): `DATABASE_URL=postgresql://root:asd@localhost:5432/wallet rake db:migrate`

## Start the app with:

`ENCRYPTION_KEY=cf6fe68cb09aca2e10b8eafead5e2738e957e1fa ENCRYPTION_SALT=cf6fe68cb09aca2e10b8eafead5e2738e957e1fa RACK_ENV=development TWITTER_APP_KEY=tw_app_key TWITTER_APP_SECRET=tw_app_secret GITHUB_APP_KEY=gh_app_key GITHUB_APP_SECRET=gh_app_secret COOKIE_SECRET=cf6fe68cb09aca2e10b8eafead5e2738e957e1fa DATABASE_URL=postgresql://root:asd@localhost:5432/wallet ruby config.ru`

To get `TWITTER_APP_KEY` and `TWITTER_APP_SECRET`, register an OAuth app at https://apps.twitter.com/
To get `GITHUB_APP_KEY` and `GITHUB_APP_SECRET`, register an OAuth app at https://github.com/settings/developers

Using Docker: `docker run --rm -it -p 4567:4567 --env-file=local.env $(docker build -q .)`

If you'd like to run postgres in docker too:
```
docker network create tipjarnetwork
docker run --name pgdb -p 5432:5432 -d --network tipjarnetwork postgres:10.1-alpine
docker run --rm -it -p 4567:4567 --env-file=local.env --network tipjarnetwork $(docker build -q .)
```

## Architecture

User is uniquely identified by `(platform, nickname)` pair. `platform` can be `twitter`, `github`, `pubkey`, `phone`, `email`, `google`, `facebook` etc. The only conceptual requirements are that (1) the said platform should provide a public, unique identifier so that others can refer to a user and (2) the user should be able to prove the ownership of that unique identifier so that s/he can claim the funds and manage the account.

A user can have many keypairs active at a time. Each keypair will have its own balance on the Indium network.

User A can visit User B's profile (URL like /u/twitter/nileshtrivedi ) and leave some funds for B. User B can then prove his identity (login using OAuth etc) and claim those funds.

A user A can also have many offers - which are just some items with an associated amount. Another user B can "buy" these offers by visiting A's profile page. This allows creation of an informal marketplace.

The private keys should NEVER be exposed - even to the owner himself. Users can withdraw the funds to another public key if they don't trust the online wallet app to store their funds securely.

The Team model is there only for platforms like Slack where usernames are only unique in the context of teams. This will be useful for future projects such as Slackbot.

## TODO
- UI / messaging on homepage and other pages, Styling for Login buttons
- Make it easy to send coins to existing contacts/friends on social networks with auto-complete
- Data field is the raw JSON field, make better UI to just send a message/payload with the payment
- After the payment, show better message with txn ID. Also make it easy to share this with the recipient
- Test & enable more OAuth platforms: Google / Facebook / GitHub (check OmniAuth for supported ones)
- Make pages responsive / mobile-friendly
- Write tests!
