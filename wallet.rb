require 'sinatra'
require 'json'
require 'omniauth'
require 'omniauth-github'
# require 'omniauth-facebook'
require 'omniauth-twitter'
require 'indium'

if ENV['RACK_ENV'].to_s == "production"
  INDIUM = Indium.new('http://rpcnode.indium.network:8545',246)
else
  INDIUM = Indium.new('http://rpcnode.indium.network:8545',246)
end

require_relative 'models/user'
require_relative 'models/keypair'
require_relative 'models/offer'

class Wallet < Sinatra::Base
  configure do
    #set :sessions, true
    set :inline_templates, true
  end
  use Rack::Session::Cookie, :key => 'rack.session',
                           :path => '/',
                           :secret => ENV["COOKIE_SECRET"]
  use OmniAuth::Builder do
    provider :github, ENV["GITHUB_APP_KEY"], ENV["GITHUB_APP_SECRET"]
    provider :twitter, ENV["TWITTER_APP_KEY"], ENV["TWITTER_APP_SECRET"]
  end

  def is_logged_in?(session)
    session[:nickname]
  end

  def current_user
    # platform = params[:platform] ? params[:platform] : session[:platform]
    User.find_by(platform: session[:platform], member: session[:member], nickname: session[:nickname].downcase, team: session[:platform]) 
  end

  def profile_path(platform, nickname)
    "/u/#{platform}/#{nickname}"
  end

  get '/auth/:platform/callback' do
    # Auth has succeeded
    oauth = request.env['omniauth.auth']
    session[:platform] = oauth['provider']
    session[:member] = oauth['uid']
    session[:nickname] = oauth['info']['nickname']
    User.create_on_login(session[:platform], session[:member], session[:nickname], oauth)
    redirect request.env['omniauth.origin'] || profile_path(session[:platform], session[:nickname])
    #erb "<h1>#{params[:platform]}</h1><pre>#{JSON.pretty_generate(oauth)}</pre>"
  end
  
  get '/auth/failure' do
    erb "<h1>Authentication Failed:</h1><h3>message:<h3> <pre>#{params}</pre>"
  end
  
  get '/auth/:platform/deauthorized' do
    erb "#{params[:platform]} has deauthorized this app."
  end

  get '/' do
    if is_logged_in?(session)
      # send him to his own profile page
      redirect profile_path(session[:platform], session[:nickname])
    else
      erb :front
    end
  end

  get '/u/:platform/:nickname' do
    if session[:platform] && session[:nickname] && session[:member] && (params[:platform] == session[:platform]) && (params[:nickname] == session[:nickname])
      # puts "user logged in and viewing own profile"
      user = User.find_by(platform: session[:platform], member: session[:member], nickname: session[:nickname].downcase, team: params[:platform])
      if !user
        redirect '/logout'
        return
      end
      me = user
      @me = me
    else
      user = User.fetch_or_create!(platform = params[:platform], nickname = params[:nickname].downcase)
      if session[:platform] && session[:nickname] && session[:member]
        me = current_user
        @me = me
      end
    end
    if user
      erb :u, :locals => {user: user, me: me}
    else
      erb "User not specified"
    end
  end

  get '/refresh' do
    throw(:halt, [401, "Not authorized\n"]) unless session[:nickname]
    if session[:platform] && session[:nickname] && session[:member]
      if current_user.allow_refresh?
        current_user.update_balance
      end
      redirect back
    else
      erb("You need to log in first")
    end
  end

  post '/pay/:platform/:nickname' do
    throw(:halt, [401, "Not authorized\n"]) unless session[:nickname]
    if params[:platform].present? && params[:nickname].present?
      # if platform = pubkey, then user row is not present in DB
      @user = User.fetch!(platform = params[:platform], nickname = params[:nickname].downcase)
      if session[:platform].present? && session[:nickname].present?
        @me = current_user
        if params.key?(:message)
          data = {"ts": Time.now.to_i, "msg": params[:message].to_s}
        else
          data = JSON.parse(params[:data])
        end  
        resp = @me.indium_pay(@user, INDIUM.eth_str_to_wei(params[:amount]), data)
        if resp.code == 200
          return erb :paid_success, :locals => {user: @user, me: @me, resp: resp, amount: params[:amount]}
        else
          return erb :paid_failure, :locals => {user: @user, me: @me, resp: resp, amount: params[:amount]}
        end
      else
        return erb("You aren't logged in.")
      end
    else
      return erb("User not specified")
    end
  end

  post '/addoffer' do
    throw(:halt, [401, "Not authorized\n"]) unless session[:nickname]
    current_user.create_offer(params)
    redirect profile_path(session[:platform], session[:nickname])
  end

  post '/deleteoffer/:offerid' do
    throw(:halt, [401, "Not authorized\n"]) unless session[:nickname]
    current_user.delete_offer(params)
    redirect profile_path(session[:platform], session[:nickname].downcase)
  end
  
  get '/logout' do
    session.clear
    redirect '/'
  end

  get '/faq' do
    return erb :faq
  end

  after do
    ActiveRecord::Base.connection.close
  end
end
