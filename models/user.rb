require 'active_record'

ActiveRecord::Base.establish_connection(ENV['DATABASE_URL'])

if ENV['RUN_MIGRATE'] == "true"
  puts "running migrations"
  require 'standalone_migrations'
  StandaloneMigrations::Tasks.load_tasks
  Rake::Task['db:migrate'].invoke
end

class User < ActiveRecord::Base
  validates :platform, presence: true, inclusion: { in: %w(twitter github) }
  validates :nickname, presence: true
  has_many :keypairs
  has_many :offers
  after_create :generate_key_pair

  def display_indium_address?
    ENV['RACK_ENV'] == 'development'
  end

  def allow_refresh?
    return true if ENV['RACK_ENV'] == 'development'
    return self.balance_at.nil? || (self.balance_at < 10.minutes.ago)
  end

  def generate_key_pair
    pubkey, privkey = INDIUM.create_keypair
    self.keypairs.create(pubkey: pubkey, privkey: privkey)
  end

  def balance
    self.keypairs.inject(0) { |sum, kp| sum + kp.fetch_balance }
  end

  def indium_pay(recipient, amount, data)
    self.keypairs.first.indium_pay(recipient.keypairs.first.pubkey, amount, data)
  end

  def self.fetch!(platform, nickname)
    if platform == "pubkey"
      pubkey = nickname.downcase
      User.new(platform: "pubkey", nickname: pubkey, member: pubkey, pubkey: pubkey)
    else
      User.find_by(platform: platform, nickname: nickname.downcase, team: platform)
    end
  end

  def self.fetch_or_create!(platform, nickname)
    if platform == "pubkey"
      pubkey = nickname.downcase
      User.new(platform: "pubkey", nickname: pubkey, member: pubkey, pubkey: pubkey)
    else
      User.find_or_create_by!(platform: platform, nickname: nickname.downcase, team: platform)
    end
  end

  def self.create_on_login(platform, member, nickname, auth_hash)
    # nickname may change over time, but member/userid cannot
    user = User.find_by_member_and_nickname_and_platform_and_team(member, nickname.downcase, platform, platform)
    if user # nothing needs to be done
      user.omniauth = auth_hash
      user.image_url = auth_hash['info']['image']
      user.save
      return user
    end
    user = User.find_by_member_and_platform_and_team(member, platform, platform)
    if user # probably nickname has changed
      puts "Nickname changed from #{user.nickname} to #{nickname}?"
      user.nickname = nickname.downcase
      user.omniauth = auth_hash
      user.image_url = auth_hash['info']['image']
      user.save
      return user
    end
    user = User.find_by_member_and_nickname_and_platform_and_team(nil, nickname.downcase, platform, platform)
    if user # was created by another user so member/userid was missing. We can update now.
      puts "User logging in himself, updating user.member now"
      user.member = member
      user.omniauth = auth_hash
      user.image_url = auth_hash['info']['image']
      user.save
      return user
    end
    # safe to create new user now
    User.create!(platform: platform, member: member, nickname: nickname.downcase, team: platform, image_url: auth_hash['info']['image'], omniauth: auth_hash)
  end

  def create_offer params
    Offer.create(title: params[:title], description: params[:description], price: INDIUM.eth_str_to_wei(params[:price]), user: self, category: 'general')
  end

  def delete_offer params 
    offer = Offer.find_by(user: self, id: params[:offerid])
    offer.discard if offer
  end

  def update_balance 
    balance = self.indium_balance
    self.balance = balance
    self.balance_at = Time.now
    self.save
  end

end