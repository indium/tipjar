require 'indium'
require 'attr_encrypted'

class Keypair < ActiveRecord::Base
	belongs_to :user
	attr_encrypted :privkey, key: ENV['ENCRYPTION_KEY'], salt: ENV['ENCRYPTION_SALT']

	def fetch_balance
		# puts "Fetching balance for #{self.pubkey} from Indium network..."
		# self.balance = INDIUM.balance(self.pubkey)
		# self.balance_at = Time.now
		# self.save
		# return self.balance
		return INDIUM.balance(self.pubkey)
	end

	def decrypted_privkey
	end

	def indium_pay(receiver_pubkey, amount, data)
		puts "Transferring #{amount} from #{self.pubkey} to #{receiver_pubkey}"
		# Below, first param = nil uses local-signing instead of signtxn service.
		# The response object will be of different type in both cases
		INDIUM.transfer(nil, self.privkey, receiver_pubkey, amount, data, gas_limit=80000, gas_price=3_141_592)
	end
end
